/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.6421139101861993, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.9942196531791907, 500, 1500, "findAllFeeGroup"], "isController": false}, {"data": [0.9636363636363636, 500, 1500, "findAllChildActiveSubsidies"], "isController": false}, {"data": [0.9857954545454546, 500, 1500, "findAllProgramBillingUpload"], "isController": false}, {"data": [0.6909090909090909, 500, 1500, "findAllChildHistorySubsidiesForBillingAdjustment"], "isController": false}, {"data": [0.005714285714285714, 500, 1500, "findAllCurrentFeeTier"], "isController": false}, {"data": [0.9807692307692307, 500, 1500, "findAllWithdrawalDraft"], "isController": false}, {"data": [0.8587570621468926, 500, 1500, "listBulkInvoiceRequest"], "isController": false}, {"data": [0.8449074074074074, 500, 1500, "bankAccountsByFkChild"], "isController": false}, {"data": [0.9909502262443439, 500, 1500, "findAllCustomSubsidies"], "isController": false}, {"data": [0.49818840579710144, 500, 1500, "findAllUploadedSubsidyFiles"], "isController": false}, {"data": [0.8904109589041096, 500, 1500, "getChildFinancialAssistanceStatus"], "isController": false}, {"data": [0.9807692307692307, 500, 1500, "getTransferDrafts"], "isController": false}, {"data": [0.9727272727272728, 500, 1500, "getAllChildDiscounts"], "isController": false}, {"data": [0.7181818181818181, 500, 1500, "findAllAbsentForVoidingSubsidyByChild"], "isController": false}, {"data": [0.8583333333333333, 500, 1500, "invoicesByFkChild"], "isController": false}, {"data": [0.002857142857142857, 500, 1500, "findAllFeeDraft"], "isController": false}, {"data": [0.0, 500, 1500, "getChildStatementOfAccount"], "isController": false}, {"data": [0.0, 500, 1500, "findAllConsolidatedRefund"], "isController": false}, {"data": [0.9683257918552036, 500, 1500, "getAdvancePaymentReceipts"], "isController": false}, {"data": [0.9864253393665159, 500, 1500, "getRefundChildBalance"], "isController": false}, {"data": [0.38321167883211676, 500, 1500, "findAllUploadedGiroFiles"], "isController": false}, {"data": [0.5823170731707317, 500, 1500, "findAllInvoice"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCreditDebitNotes"], "isController": false}, {"data": [0.7272727272727273, 500, 1500, "bankAccountInfoByIDChild"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3652, 0, 0.0, 1656.3691128148935, 8, 26939, 453.0, 4797.800000000001, 6244.349999999997, 23502.389999999992, 11.681092111744423, 242.01275440439863, 20.645524923474774], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllFeeGroup", 173, 0, 0.0, 198.98843930635837, 11, 819, 185.0, 379.6, 432.0, 693.1999999999985, 0.5890344261681097, 0.5602729795778699, 0.6350527407124933], "isController": false}, {"data": ["findAllChildActiveSubsidies", 55, 0, 0.0, 280.0727272727272, 31, 728, 282.0, 485.59999999999997, 538.3999999999997, 728.0, 0.20367202138926538, 0.2293226898500974, 0.332160425507884], "isController": false}, {"data": ["findAllProgramBillingUpload", 176, 0, 0.0, 245.51136363636363, 23, 700, 231.0, 434.3, 464.3, 659.9599999999995, 0.5881409399561568, 0.7940150706938727, 0.8776165588408277], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 55, 0, 0.0, 585.7636363636365, 169, 1600, 551.0, 828.8, 1072.3999999999985, 1600.0, 0.20336025083562576, 0.29011332943251394, 0.36581013871017837], "isController": false}, {"data": ["findAllCurrentFeeTier", 175, 0, 0.0, 2210.4857142857136, 1381, 3489, 2082.0, 2890.2000000000003, 3074.7999999999997, 3432.7600000000007, 0.5916019566812145, 4.348997377301754, 1.4980701891351456], "isController": false}, {"data": ["findAllWithdrawalDraft", 104, 0, 0.0, 213.66346153846155, 9, 1037, 186.5, 374.0, 492.0, 1023.2500000000008, 0.3486691900481767, 0.17603708130362047, 1.1311318841211357], "isController": false}, {"data": ["listBulkInvoiceRequest", 177, 0, 0.0, 402.1751412429378, 51, 947, 412.0, 624.4000000000001, 696.0, 806.5999999999998, 0.5901003170538992, 0.9072050061010372, 0.8943707930348159], "isController": false}, {"data": ["bankAccountsByFkChild", 216, 0, 0.0, 383.4305555555556, 40, 1124, 375.5, 678.5, 792.8999999999999, 997.9099999999987, 0.7441886104689422, 1.078411338195825, 1.712941948120407], "isController": false}, {"data": ["findAllCustomSubsidies", 221, 0, 0.0, 197.23981900452483, 29, 683, 184.0, 376.8, 412.5999999999999, 659.3400000000001, 0.7379088131688343, 0.8847412675921134, 0.923106630536403], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 276, 0, 0.0, 897.5181159420289, 328, 1684, 867.5, 1240.5000000000002, 1367.8499999999995, 1600.5900000000006, 0.9214097569281, 81.35875298060199, 1.3281257824471442], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 219, 0, 0.0, 339.29223744292227, 20, 1325, 322.0, 647.0, 760.0, 951.6, 0.7319029476639262, 0.526769992605775, 0.9606226188089031], "isController": false}, {"data": ["getTransferDrafts", 104, 0, 0.0, 204.81730769230774, 11, 934, 174.5, 349.0, 470.75, 930.8500000000001, 0.34890079475575264, 0.17445039737787632, 0.5983103472569352], "isController": false}, {"data": ["getAllChildDiscounts", 55, 0, 0.0, 263.99999999999994, 25, 532, 275.0, 428.2, 509.99999999999994, 532.0, 0.203131924951987, 0.12418025880853893, 0.33485028253804106], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 55, 0, 0.0, 564.7454545454544, 46, 1114, 587.0, 920.8, 1058.8, 1114.0, 0.20330312125737438, 0.16359548038679345, 0.27080611073736194], "isController": false}, {"data": ["invoicesByFkChild", 60, 0, 0.0, 452.8833333333332, 129, 1257, 414.0, 816.9, 890.5999999999999, 1257.0, 0.20169829968333366, 0.5748303055477118, 0.586382654450473], "isController": false}, {"data": ["findAllFeeDraft", 175, 0, 0.0, 3339.577142857143, 1431, 5465, 3371.0, 4308.400000000001, 4478.799999999999, 5021.9200000000055, 0.5909886057396814, 0.33589391459032675, 0.9418880903976172], "isController": false}, {"data": ["getChildStatementOfAccount", 218, 0, 0.0, 5532.83486238532, 3873, 8145, 5563.5, 6356.0, 6451.249999999996, 8043.300000000001, 0.7286778174428088, 1.0928698363733904, 1.1883710499311433], "isController": false}, {"data": ["findAllConsolidatedRefund", 99, 0, 0.0, 4204.8888888888905, 2796, 5109, 4221.0, 4738.0, 4972.0, 5109.0, 0.3464288088797752, 0.4791691088731265, 0.5988076090988301], "isController": false}, {"data": ["getAdvancePaymentReceipts", 221, 0, 0.0, 225.52941176470586, 13, 925, 205.0, 440.6000000000001, 531.1999999999996, 727.6600000000001, 0.7388042736985678, 1.1110923647419868, 1.17891228830416], "isController": false}, {"data": ["getRefundChildBalance", 221, 0, 0.0, 177.4479638009049, 8, 1169, 161.0, 351.20000000000005, 401.5999999999997, 1053.7200000000007, 0.7385820561322363, 0.7046822937902294, 1.1569195488633859], "isController": false}, {"data": ["findAllUploadedGiroFiles", 274, 0, 0.0, 1299.2299270072988, 584, 2145, 1274.0, 1669.0, 1833.25, 2016.75, 0.9285711865038617, 157.33012061258586, 1.3293802338033802], "isController": false}, {"data": ["findAllInvoice", 164, 0, 0.0, 8737.5731707317, 25, 26939, 458.5, 24582.5, 25259.5, 26833.05, 0.5257168501867256, 0.942803624521165, 1.6865663717523363], "isController": false}, {"data": ["findAllCreditDebitNotes", 104, 0, 0.0, 7551.605769230771, 4577, 10642, 7459.5, 9006.0, 10171.5, 10639.8, 0.3432445188140902, 0.657107492466773, 0.8021329428926932], "isController": false}, {"data": ["bankAccountInfoByIDChild", 55, 0, 0.0, 560.6, 100, 1084, 561.0, 896.4, 1045.1999999999998, 1084.0, 0.2026954813649145, 0.28992435888340584, 0.46675385259616053], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3652, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
